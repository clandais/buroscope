﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IntroMovie : MonoBehaviour {

    public GameObject mainMenu;

#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)
    MovieTexture movie;
#endif

    void Start ()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)

        movie = Resources.Load("Videos/intro3_1_1_1") as MovieTexture;
        GetComponent<RawImage>().texture = movie;

#endif

        StartCoroutine(PlayMovie());

    }


    IEnumerator PlayMovie()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)
        if (movie)
        {
            movie.Play();
            while (movie.isPlaying) yield return null;
        }
#elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
        Handheld.PlayFullScreenMovie("intro_android.mp4", Color.white, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.Fill);
        yield return new WaitForEndOfFrame();
#endif
        mainMenu.SetActive(true);
        transform.parent.gameObject.SetActive(false);

    }
    
}
