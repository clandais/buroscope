﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour {



    [SerializeField]
    private GameObject playScreen, creditScreen;


    void OnDisable()
    {
       // play.onClick.RemoveAllListeners();
    }

    public void MainMenuForCredit()
    {
        creditScreen.SetActive(false);
        gameObject.SetActive(true);
    }

    public void Play()
    {
        playScreen.SetActive(true);
        gameObject.SetActive(false);
        
    }

    public void Credit()
    {
        creditScreen.SetActive(true);
        gameObject.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
