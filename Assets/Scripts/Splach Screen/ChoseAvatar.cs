﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;

public class ChoseAvatar : MonoBehaviour {

    [SerializeField]
    private Button back;
    [SerializeField]
    private Button start;
    private bool startButtonActivated = false;
    [SerializeField]
    private GameObject mainMenu;

    [SerializeField]
    private GameObject loadingMenu;

    private int choseavatarIndex = 1;
    [SerializeField]
    private GameObject haloMale;
    [SerializeField]
    private GameObject haloFemale;
    [SerializeField]
    private List<GameObject> persosPrefabs;

   // [SerializeField]
    


    private int selectedIndex = -1;
    private const int MALE_INDEX = 0;
    private const int FEMALE_INDEX = 1;

    /*
     * 
     * No save files for now
    [SerializeField]
    private FindAllSaveFiles files;
    */




    void Start()
    {
        back.onClick.AddListener(BackToMainMenu);
        ActivateStartButton(false);
    }

    /// <summary>
    /// Back to main menu callback
    /// </summary>
    void BackToMainMenu()
    {
        gameObject.SetActive(false);
        mainMenu.SetActive(true);
    }

    /// <summary>
    /// Start app callback
    /// </summary>
    void StartApp()
    {
        GameObject characterChoice = GameObject.Find("PersistentCharacterChoice");
        characterChoice.GetComponent<PersistentCharacterChoice>().characterPrefab = persosPrefabs[selectedIndex];
        DontDestroyOnLoad(characterChoice);

        StartCoroutine(LoadSceneAsync());

    }

    IEnumerator LoadSceneAsync()
    {
        gameObject.SetActive(false);
        loadingMenu.SetActive(true);

        AsyncOperation async = SceneManager.LoadSceneAsync("SceneOne");
        while (!async.isDone) yield return null;
    }

    /// <summary>
    /// Toggles the active state of the Start button
    /// </summary>
    /// <param name="activate"></param>
    void ActivateStartButton(bool activate)
    {
        if (!activate)
        {
            start.GetComponent<Image>().color = Color.gray;
            startButtonActivated = false;
        }
        else
        {
            if (!startButtonActivated)
            {
                startButtonActivated = true;
                start.GetComponent<Image>().color = Color.white;
                start.onClick.AddListener(StartApp);
            }
        }

    }

	void Update ()
    {
	    if (Input.GetMouseButton(0) || Input.touchCount > 0)
        {
            RaycastHit hit;
            Ray ray;
            #if (UNITY_EDITOR || UNITY_STANDALONE_WIN)
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //for touch device
            #elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
                ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            #elif UNITY_WEBPLAYER
                if (Input.touchCount > 0)
                    ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                else ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            #endif

            //Debug.DrawRay(mousePos, Vector3.back*100, Color.blue);
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.transform.CompareTag("ChoseAvatar"))
                {
                    choseavatarIndex = int.Parse(hit.transform.name[0].ToString()) - 1;

                    haloMale.GetComponent<Image>().color= Color.clear;
                    haloFemale.GetComponent<Image>().color = Color.clear;

                    if (choseavatarIndex == MALE_INDEX)
                    {
                        if (haloMale.GetComponent<Image>().color != Color.white) haloMale.GetComponent<Image>().color = Color.white; //SetActive(true);
                        selectedIndex = MALE_INDEX;
                    }
                    else if (choseavatarIndex == FEMALE_INDEX)
                    {
                        if (haloFemale.GetComponent<Image>().color != Color.white) haloFemale.GetComponent<Image>().color = Color.white; //.SetActive(true);
                        selectedIndex = FEMALE_INDEX;
                    }

                    ActivateStartButton(true);

                }
            }            
        }
	}

}
