﻿using System.Xml;
using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public static class XmlParser
{

    public static XmlNode Parse(string documentAsString, string xpath)
    {
        XmlDocument document = new XmlDocument();
        document.LoadXml(documentAsString);
        return document.DocumentElement.SelectSingleNode(xpath);

    }

    public static string GetInnerText(XmlNode node, string nodeName)
    {
        XmlNode retNode = node.SelectSingleNode(nodeName);
        if (retNode == null) return null;
        return Regex.Replace( retNode.InnerText, @"\t|\r", "").Trim();
    }

    public static string GetAttributeValue(XmlNode node, string nodeName, string attributeName)
    {
        XmlNode targetNode = node.SelectSingleNode(nodeName);
        if (targetNode == null || targetNode.Attributes[attributeName] == null) return null;
        return targetNode.Attributes[attributeName].Value;
    }
	
}
