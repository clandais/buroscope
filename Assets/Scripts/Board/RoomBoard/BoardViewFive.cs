﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System;

public class BoardViewFive : MonoBehaviour, IPointerClickHandler
{

    public GameObject nextView;
    public GameObject lastView;

    public Text informations;
    public Text title;
    public Question[] questions;

    QuestionGroup[] questionGroups;
    int formationId;
    TextAsset xmlDocument;
    int currentQuestionNum;
    int totalQuestionsNum = 3;


    public void Initialize(TextAsset xmlDocument, int formationId)
    {

        this.formationId = formationId;
        this.xmlDocument = xmlDocument;

        questionGroups = new QuestionGroup[3];
        currentQuestionNum = 0;

        XmlNode viewNode = XmlParser.Parse(xmlDocument.text, "/data/view[@id='5']/formation[@id='" + formationId + "']");


        for (int i = 0; i < 3; i++)
        {
            string baseXPath = "question[@id='" + (i + 1) + "']/";
            questionGroups[i].informations = XmlParser.GetInnerText(viewNode, baseXPath + "informations");
            questionGroups[i].questions = new Question[3];
            questionGroups[i].title = XmlParser.GetInnerText(viewNode, baseXPath + "title");
            questionGroups[i].isCorrect = false;
            for (int j = 0; j < questionGroups[i].questions.Length; j++)
            {
                questionGroups[i].questions[j].contentStr = XmlParser.GetInnerText(viewNode, baseXPath + "box[@id='" + (j + 1) + "']");
                questionGroups[i].questions[j].isTrue = bool.Parse(
                    XmlParser.GetAttributeValue(viewNode, baseXPath + "box[@id='" + (j + 1) + "']", "isTrue")
                    );
            }
        }




        //initialize first question

        PopulateFields(currentQuestionNum);


    }

    private void PopulateFields(int questioNum)
    {
        informations.text = questionGroups[questioNum].informations;
        title.text = questionGroups[questioNum].title;
        for (int i = 0; i < 3; i++)
        {
            questions[i].toggle.isOn = false;
            questions[i].content.text = questionGroups[questioNum].questions[i].contentStr;
            questions[i].isTrue = questionGroups[questioNum].questions[i].isTrue;
        }
    }


	public void GoToResult()
    {
        //check if answer is OK

        for (int i = 0; i < 3; i++)
        {
            if (questions[i].toggle.isOn && questions[i].isTrue)
            {
                questionGroups[currentQuestionNum].isCorrect = true;
                continue;
            }
                
        }
       

        currentQuestionNum++;

        if (currentQuestionNum < totalQuestionsNum)
        {
            //re populate questions fields
            PopulateFields(currentQuestionNum);
            return;
        }

        //count score
        int result = 0;
        for (int i = 0; i < questionGroups.Length; i++)
            result += questionGroups[i].isCorrect ? 1 : 0;


        nextView.SetActive(true);
        nextView.GetComponent<BoardViewSix>().Initialize(xmlDocument, result, formationId);
        gameObject.SetActive(false);

    }


    private void GoBack()
    {
        gameObject.SetActive(false);
        lastView.SetActive(true);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
            GoBack();
    }
}
