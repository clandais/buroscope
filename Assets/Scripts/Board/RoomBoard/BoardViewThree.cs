﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;

public class BoardViewThree : MonoBehaviour, IPointerClickHandler
{
    public GameObject viewTwo;
    public List<ObjectifDetail> details;

    public Text contentPanel;
    public Scrollbar scrollBar;


    public void Initialize(TextAsset xmlDocument, int formationId)
    {
        //reset scroll
        scrollBar.value = 1f;

        XmlNode viewNode = XmlParser.Parse(xmlDocument.text, "/data/view[@id='3']/formation[@id='" + formationId + "']");

        if (viewNode == null)
        {
            Debug.LogError("XmlNode was null");
            return;
        }


        contentPanel.text = "";

        int i = 1;
        foreach (ObjectifDetail item in details)
        {
            string objectifId = "objectif[@id = '" + i + "']";
            string title =  XmlParser.GetInnerText(viewNode, objectifId + "/title");
            string content = XmlParser.GetInnerText(viewNode, objectifId + "/content");

            if (title == null) item.title.text = "";
            else contentPanel.text +=  "\t<color='red'>" + title + "</color>\n";

            if (content == null) item.content.text = "";
            else contentPanel.text += content + "\n\n";
            i++;

        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
      //  if (eventData.button == PointerEventData.InputButton.Right)
            GoToViewTwo();
    }

    void GoToViewTwo()
    {
        gameObject.SetActive(false);
        viewTwo.SetActive(true);
    }
}
