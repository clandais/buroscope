﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;
using System.Xml;

public class BoardViewSix : MonoBehaviour, IPointerClickHandler {

    public GameObject viewOne;
    public GameObject viewFive;
    public Text score;
    public ScorePanel scorePanel;
    public int courseCount;


    int formationId;
    int formationGroup;
    float QCMScore;

    public void Initialize(TextAsset xmlDocument, int score, int formationId)
    {
        XmlNode dataNode = XmlParser.Parse(xmlDocument.text, "/data");
        formationGroup = int.Parse(
            XmlParser.GetInnerText(dataNode, "formation-group")
            );

        this.formationId = formationId;
        this.score.text = score + " sur 3";

        QCMScore = (float)score / 3f;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
            GoBack();
    }

    private void GoBack()
    {
        gameObject.SetActive(false);
        viewFive.SetActive(true);
    }

    public void BackToViewOneAndUpdateScores()
    {
        scorePanel.UpdateScores(formationGroup, formationId, QCMScore, courseCount);

        gameObject.SetActive(false);
        viewOne.SetActive(true);
    }
}
