﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public struct BoardTile
{
    public Image image;
    public Image logo;
    public Text text;
}
