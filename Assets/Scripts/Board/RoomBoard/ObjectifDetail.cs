﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

[Serializable]
public struct ObjectifDetail
{
    public Text title;
    public Text content;
}
