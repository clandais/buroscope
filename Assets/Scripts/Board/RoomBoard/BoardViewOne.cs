﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class BoardViewOne : MonoBehaviour {

    public GameObject viewTwo;
    public TextAsset xmlDocument;
    public List<BoardTile> tiles;



    
	// Use this for initialization
	void Start () {

        XmlNode viewNode = XmlParser.Parse(xmlDocument.text, "/data/view[@id='1']");

        int id = 0;
        foreach (BoardTile tile in tiles)
        {
            //color node
            Color color = new Color();
            string hexColor = XmlParser.GetInnerText(viewNode, "tile[@id='" + id + "']/color");
            ColorUtility.TryParseHtmlString(hexColor, out color);
            tile.image.color = color;

            //logo node
            string logoName = XmlParser.GetInnerText(viewNode, "tile[@id='" + id + "']/logo");
            tile.logo.sprite = Resources.Load<Sprite>("Sprites/Logo/" + logoName);

            //text node
            tile.text.text = XmlParser.GetInnerText(viewNode, "tile[@id='" + id + "']/text");

            id++;
        }

        //special case : 3 tiles

        if (tiles.Count < 4)
        {
            GameObject tile =  transform.GetChild(0).GetChild(3).gameObject;
            tile.SetActive(false);
        }

        //special cas: 2 tiles......
        if (tiles.Count < 3)
        {
            GameObject tile = transform.GetChild(0).GetChild(2).gameObject;
            tile.SetActive(false);
        }
	}


    void TileClicked(int id)
    {
        gameObject.SetActive(false);
        viewTwo.SetActive(true);
        viewTwo.GetComponent<BoardViewTwo>().Initialize(xmlDocument, id);
    }


}
