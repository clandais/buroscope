﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Xml;

public class BoardViewFour : MonoBehaviour, IPointerClickHandler
{

    public GameObject presentationView;
    public GameObject QCMView;

    public Text title;
    public Text content;

    TextAsset xmlDocument;
    int formationId;


    public void Initialize(TextAsset xmlDocument, int formationId)
    {
        this.xmlDocument = xmlDocument;
        this.formationId = formationId;

        XmlNode viewNode = XmlParser.Parse(xmlDocument.text, "/data/view[@id='4']/formation[@id='" + formationId + "']");

        if (viewNode == null)
        {
            Debug.LogError("XmlNode was null");
            return;
        }


        title.text = XmlParser.GetInnerText(viewNode, "title");
        content.text = XmlParser.GetInnerText(viewNode, "content");
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
            GoToViewProgramView();
    }

    private void GoToViewProgramView()
    {
        gameObject.SetActive(false);
        presentationView.SetActive(true);
    }

    public void GoToQCM()
    {
        gameObject.SetActive(false);
        QCMView.SetActive(true);
        QCMView.GetComponent<BoardViewFive>().Initialize(xmlDocument, formationId);
    }

}
