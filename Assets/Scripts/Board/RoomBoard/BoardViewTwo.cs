﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Xml;
using System;

public class BoardViewTwo : MonoBehaviour, IPointerClickHandler
{
    public GameObject lastView;
    public GameObject programView;
    public GameObject QCMView;

    public Text _public;
    public Text prerequis;
    public Text duree;
    public Text organisation;
    public Text evaluation;
    public Text objectifs;


    TextAsset xmlDocument;
    int formationId;

    public void Initialize(TextAsset xmlDocument, int formationId)
    {
        this.xmlDocument = xmlDocument;
        this.formationId = formationId;

        XmlNode viewNode = XmlParser.Parse(xmlDocument.text, "/data/view[@id='2']/formation[@id='" + formationId +"']");

        _public.text = XmlParser.GetInnerText(viewNode, "public");
        prerequis.text = XmlParser.GetInnerText(viewNode, "prerequis");
        duree.text = XmlParser.GetInnerText(viewNode, "duree");
        organisation.text = XmlParser.GetInnerText(viewNode, "organisation");
        evaluation.text = XmlParser.GetInnerText(viewNode, "evaluation");
        objectifs.text = XmlParser.GetInnerText(viewNode, "objectifs");
    }

    public void GoToProgramme()
    {
        gameObject.SetActive(false);
        programView.SetActive(true);
        programView.GetComponent<BoardViewThree>().Initialize(xmlDocument, formationId);
    }


    public void GoToQCMIntro()
    {
        gameObject.SetActive(false);
        QCMView.SetActive(true);
        QCMView.GetComponent<BoardViewFive>().Initialize(xmlDocument, formationId);

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //right click
        if (eventData.button == PointerEventData.InputButton.Right)
            GotoViewOne();
    }


    void GotoViewOne()
    {
        gameObject.SetActive(false);
        lastView.SetActive(true);
    }
}
