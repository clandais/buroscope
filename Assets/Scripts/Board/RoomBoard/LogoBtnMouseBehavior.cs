﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class LogoBtnMouseBehavior : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    public int id;

    Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        animator.SetBool("Grow", true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        animator.SetBool("Grow", false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SendMessageUpwards("TileClicked", id);
    }
}
