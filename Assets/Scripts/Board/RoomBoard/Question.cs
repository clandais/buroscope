﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


[System.Serializable]
public struct QuestionGroup
{
    public Question[] questions;
    public string informations;
    public string title;
    public bool isCorrect;
}

[System.Serializable]
public struct Question
{

    public Toggle toggle;
    public Text content;
    public string contentStr;
    public bool isTrue;
}
