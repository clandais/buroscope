﻿using UnityEngine;
using System.Collections;

public class ButtonPanelController : MonoBehaviour {

	public void DisableButtonInteraction()
    {
        AccueilButtons[] accueilButtons = transform.GetComponentsInChildren<AccueilButtons>();

        foreach (AccueilButtons accueilButton in accueilButtons)
            accueilButton.interactionsEnabled = false;
    }

    public void EnableButtonInteraction()
    {
        AccueilButtons[] accueilButtons = transform.GetComponentsInChildren<AccueilButtons>();

        foreach (AccueilButtons accueilButton in accueilButtons)
            accueilButton.interactionsEnabled = true;
    }
}
