﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class GoTo : MonoBehaviour, IPointerClickHandler
{
    public GameObject   pathIndicator;
    public Transform    destinationWaypoint;
    public Color32      pathColor;

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        pathIndicator.GetComponent<PathIndicator>().DrawPath(destinationWaypoint, pathColor);
        destinationWaypoint.GetComponent<WaypointTriggers>().hasBeenTriggered = false;
    }
}
