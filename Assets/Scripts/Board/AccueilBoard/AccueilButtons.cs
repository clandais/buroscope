﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class AccueilButtons : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler, IPointerClickHandler
{

    [SerializeField]
    private GameObject buttonLabelElement;
    [SerializeField]
    private string buttonLabel;
    [SerializeField]
    private GameObject popupPanel;

    public bool interactionsEnabled;

    private void Start()
    {
        interactionsEnabled = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (interactionsEnabled)
        {
            GetComponent<Image>().color = Color.grey;
            buttonLabelElement.SetActive(true);
            buttonLabelElement.GetComponent<Text>().text = buttonLabel;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(interactionsEnabled)
        {
            GetComponent<Image>().color = Color.white;
            buttonLabelElement.SetActive(false);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (interactionsEnabled)
        {
            GetComponent<Image>().color = Color.white;
            popupPanel.SetActive(true);
            SendMessageUpwards("DisableButtonInteraction");
        }
        
    }
}
