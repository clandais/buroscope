﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PopupCloseButton : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler, IPointerClickHandler
{
    [SerializeField]
    private GameObject popupPanel;
    [SerializeField]
    private Color defaultColor;
    [SerializeField]
    private Color hoverColor;

    [SerializeField]
    private GameObject buttonPanel;

    public void OnPointerEnter(PointerEventData eventData)
    {
        GetComponent<Image>().color = hoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponent<Image>().color = defaultColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GetComponent<Image>().color = defaultColor;
        popupPanel.SetActive(false);

        buttonPanel.GetComponent<ButtonPanelController>().EnableButtonInteraction();
    }
}
