﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathIndicator : MonoBehaviour {

    private Vector3 target;
    private UnityEngine.AI.NavMeshAgent agent;
    private List<Transform> dots;
    private Color32 dotColor;

    public GameObject gameCharacter;
    public Transform dotPrefab;




    void OnEnable()
    {
        EventManager.StartListening("RemovePathIndicators", RemovePathIndicators);
    }

    void OnDisable()
    {
        EventManager.StartListening("RemovePathIndicators", RemovePathIndicators);
    }



    private void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.enabled = false;
        dots = new List<Transform>();

    }


    private void Update()
    {
        if(gameCharacter.activeInHierarchy)
            transform.position = gameCharacter.transform.position;
    }

    public void DrawPath(Transform to, Color32 dotColor)
    {
        RemovePathIndicators();
        agent.enabled = true;
        target = to.position;
        this.dotColor = dotColor;
        StartCoroutine(getPath());
    }

    private IEnumerator getPath()
    {
       // line.SetPosition(0, transform.position);

        agent.SetDestination(target);
        while (agent.pathPending)
            yield return new WaitForEndOfFrame();

        DrawPath(agent.path);

        agent.enabled = false;
       // agent.Stop();//add this if you don't want to move the agent
    }


    private void DrawPath(UnityEngine.AI.NavMeshPath path)
    {
        if (path.corners.Length < 2) //if the path has 1 or no corners, there is no need
            return;

       // line.SetVertexCount(path.corners.Length); //set the array of positions to the amount of corners


        for (int i = 0; i < path.corners.Length - 1; i++)
        {

            Vector3 start = path.corners[i];
            Vector3 end = path.corners[i + 1];
            Vector3 pathDirection = (end - start).normalized;
            Transform dot;
            int dotPos = 1;
            float spacing = .5f;


            if (Vector3.Distance(start, end) <= spacing) continue;

            dot = Instantiate<Transform>(dotPrefab);
            dot.gameObject.GetComponent<Renderer>().material.color = dotColor;
            dot.position = start + (pathDirection * dotPos * spacing);
            dots.Add(dot);

            while (Vector3.Distance(dot.position, end) > spacing)
            {
                dot = Instantiate<Transform>(dotPrefab);
                dot.gameObject.GetComponent<Renderer>().material.color = dotColor;
                dot.position = start + (pathDirection * dotPos * spacing);
                dots.Add(dot);
                dotPos++;
            }
           

           
        }
    }


    void RemovePathIndicators()
    {
        foreach (Transform dot in dots)
        {
            Object.Destroy(dot.gameObject);
        }

        dots.Clear();
    }
}
