﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Xml;

public class PanelContentFromXml : MonoBehaviour {


    public TextAsset xmlAsset;
    public Text title;
    public Text subTitle;
    public Text content;
    public Text gotoBtnText;

	// Use this for initialization
	void Start () {

        string xml = xmlAsset.text;
        XmlNode panelNode = XmlParser.Parse(xml, "/data/panel[@id = '" + gameObject.name + "']");

        title.text = XmlParser.GetInnerText(panelNode, "title");
        subTitle.text = XmlParser.GetInnerText(panelNode, "subtitle");
        content.text = XmlParser.GetInnerText(panelNode, "content");
        gotoBtnText.text = XmlParser.GetInnerText(panelNode, "goto");

    }
}
