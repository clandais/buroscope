﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFacingBillboard : MonoBehaviour {


    public Camera _camera;

	void Update ()
    {
        transform.LookAt(
            transform.position - _camera.transform.rotation * Vector3.forward,
            _camera.transform.rotation * Vector3.up);
	}
}
