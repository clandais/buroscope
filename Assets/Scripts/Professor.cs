﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class Professor : MonoBehaviour, IPointerClickHandler
{
    public int iDListNodes;
    public Vector3 finalRotation;
    public float distanceToTalking = 3;
    public Transform player;
    public GameObject returnButton;

    private enum NodeId
    {
        secreter = 0
    }

    void Update()
    {
        
        if (player.GetComponent<MovePerso>().professor && transform == player.GetComponent<MovePerso>().professorT)
            if ((Vector3.Distance(player.position, transform.position)) < distanceToTalking)
            {
                if (Camera.main.transform.parent.GetComponent<CameraMove>().enabled)
                {
                    Camera.main.transform.parent.GetComponent<CameraMove>().enabled = false;
                    Camera.main.transform.parent.GetComponent<MoveCameraToRail>().enabled = true;
                    Camera.main.transform.parent.GetComponent<MoveCameraToRail>().currentNode = 0;
                    Camera.main.transform.parent.GetComponent<MoveCameraToRail>().idListNodes = iDListNodes;
                    Camera.main.transform.parent.GetComponent<MoveCameraToRail>().finalRotation = finalRotation;
                    player.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(player.position);
                    player.GetComponent<UnityEngine.AI.NavMeshAgent>().Stop();
                    player.GetComponent<MovePerso>().professor = false;
                    player.GetComponent<MovePerso>().professorT = null;
                    player.GetComponent<MovePerso>().canMove = false;
                    player.gameObject.SetActive(false);

                    returnButton.SetActive(true);
                }
            }
            
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        player.GetComponent<MovePerso>().professor = true;
        player.GetComponent<MovePerso>().professorT = transform;
        player.GetComponent<MovePerso>().Move(eventData.pointerCurrentRaycast.worldPosition);

        if(iDListNodes == (int)NodeId.secreter)
            EventManager.TriggerEvent("AccueilScreen");

    }
}
