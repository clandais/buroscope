﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class RaycastingHandler : MonoBehaviour, IPointerClickHandler, IDragHandler, IPointerUpHandler, IPointerDownHandler
{

    public GameObject playerCharacter;

    public void OnDrag(PointerEventData eventData)
    {
        playerCharacter.GetComponent<MovePerso>().Move(eventData.pointerCurrentRaycast.worldPosition);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        playerCharacter.GetComponent<MovePerso>().Move(eventData.pointerCurrentRaycast.worldPosition);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        playerCharacter.GetComponent<MovePerso>().Move(eventData.pointerCurrentRaycast.worldPosition);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        return;
    }
}
