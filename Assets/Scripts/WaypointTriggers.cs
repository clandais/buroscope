﻿using UnityEngine;
using System.Collections;

public class WaypointTriggers : MonoBehaviour {

    public GameObject mainCharacter;
    public float triggerArea = 10f;
    public GameObject exitWaypoint;

    public bool hasBeenTriggered = false;


	void FixedUpdate()
    {

        if ( !hasBeenTriggered && Vector3.Distance(transform.position, mainCharacter.transform.position) < triggerArea)
        {
            EventManager.TriggerEvent("RemovePathIndicators");

            switch (gameObject.name)
            {
                case "CassiopeeWaypoint":
                    EventManager.TriggerEvent("CassiopeeEnter");
                    
                    break;

                case "PallasWaypoint":
                    EventManager.TriggerEvent("PallasEnter");
                    break;

                case "OrionWaypoint":
                    EventManager.TriggerEvent("OrionEnter");
                    break;

                case "AndromèdeWaypoint":
                    EventManager.TriggerEvent("AndromedeEnter");
                    break;

                case "VégaWaypoint":
                    EventManager.TriggerEvent("VegaEnter");
                    break;

                case "SiriusWaypoint":
                    EventManager.TriggerEvent("SiriusEnter");
                    break;

                case "DionéWaypoint":
                    EventManager.TriggerEvent("DioneEnter");
                    break;



                default:
                    break;
            }

            hasBeenTriggered = true;
        }
    }


    void OnDrawGizmos()
    {
        Gizmos.color = new Color32(255, 0, 0, 64);
        Gizmos.DrawSphere(transform.position, triggerArea);
    }
}
