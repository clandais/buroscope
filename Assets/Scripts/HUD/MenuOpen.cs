﻿using UnityEngine;
using System.Collections;

public class MenuOpen : MonoBehaviour {


    public GameObject menuOpened;
    public GameObject menuClosed;

    void Start()
    {
        //menuOpened.GetComponent<Animator>().SetBool("IsHidden", true);
    }


    public void Show()
    {
        menuOpened.SetActive(true);
        menuClosed.SetActive(false);
        menuOpened.GetComponent<Animator>().SetBool("IsHidden", false);

    }

    public void Hide()
    {
        menuOpened.GetComponent<Animator>().SetBool("IsHidden", true);
    }

    public void OnCloseAnimationComplete()
    {
        menuOpened.SetActive(false);
        menuClosed.SetActive(true);
             
    }
}
