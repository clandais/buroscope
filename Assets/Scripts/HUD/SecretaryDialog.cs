﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System;

public class SecretaryDialog : MonoBehaviour
{


    public GameObject bubble;
    public Text bubbleText;
    private Animator animator;

    private Dictionary<string, UnityAction> eventDict;

    void Awake()
    {
        eventDict = new Dictionary<string, UnityAction>();
        eventDict.Add("CassiopeeEnter", CassiopeeEnter);
        eventDict.Add("PallasEnter", PallasEnter);
        eventDict.Add("OrionEnter", OrionEnter);
        eventDict.Add("AndromedeEnter", AndromedeEnter);
        eventDict.Add("VegaEnter", VegaEnter);
        eventDict.Add("SiriusEnter", SiriusEnter);
        eventDict.Add("DioneEnter", DioneEnter);
        eventDict.Add("OnRoomExit", OnRoomExit);
        


        bubble.SetActive(false);
    }

	void OnEnable()
    {
        foreach (KeyValuePair<string, UnityAction> pair in eventDict)
            EventManager.StartListening(pair.Key, pair.Value);
    }

    void Start()
    {
        bubble.SetActive(true);
        animator = GetComponent<Animator>();
        animator.SetBool("big", true);
    }

    void OnDisable()
    {
        foreach (KeyValuePair<string, UnityAction> pair in eventDict)
            EventManager.StopListening(pair.Key, pair.Value);
    }


    public void BubbleClicked()
    {
        if (animator.GetBool("big")) animator.SetBool("big", false);
        if (animator.GetBool("medium")) animator.SetBool("medium", false);
        //if (animator.GetBool("medium")) animator.SetBool("fadeout", true);
    }

    public void EndWelcomeCallback()
    {
        animator.SetBool("medium", true);
        bubbleText.text = "Bonjour et bienvenu chez Buroscope.\n" +
                          "Je vous invite à cliquer sur mon avatar à l'accueil près de vous pour découvrir les formations proposées.";
    }

    private void CassiopeeEnter()
    {
        animator.SetBool("medium", true);
        bubbleText.text = "Cassiopee Welcome";
    }


    private void PallasEnter()
    {
        animator.SetBool("medium", true);
        bubbleText.text = "Pallas Welcome";
    }

    private void VegaEnter()
    {
        animator.SetBool("medium", true);
        bubbleText.text = "Vega Welcome";
    }

    private void AndromedeEnter()
    {
        animator.SetBool("medium", true);
        bubbleText.text = "Andromède Welcome";
    }

    private void OrionEnter()
    {
        animator.SetBool("medium", true);
        bubbleText.text = "Orion Welcome";
    }

    private void SiriusEnter()
    {
        animator.SetBool("medium", true);
        bubbleText.text = "Sirius Welcome";
    }

    private void DioneEnter()
    {
        animator.SetBool("medium", true);
        bubbleText.text = "Dioné Welcome";
    }

    private void OnRoomExit()
    {
        animator.SetBool("medium", true);
        bubbleText.text = "Reminder to click on portrait";
    }
}
