﻿using UnityEngine;
using System.Collections;

public class Return : MonoBehaviour {


    public GameObject player;
    public GameObject listPNJ;
    public GameObject formationPanel;

	public void Quit()
    {
      //  inFrontOfBoard = false;
        Camera.main.transform.parent.GetComponent<CameraMove>().enabled = true;
        Camera.main.transform.parent.GetComponent<MoveCameraToRail>().enabled = false;
        player.SetActive(true);
        GameObject.FindGameObjectWithTag("Player").GetComponent<UnityEngine.AI.NavMeshAgent>().Resume();
        GameObject.FindGameObjectWithTag("Player").GetComponent<MovePerso>().canMove = true;
        listPNJ.gameObject.SetActive(true);

        transform.gameObject.SetActive(false);
    }

    public void QuitOverlay()
    {
        formationPanel.SetActive(false);
    }
}
