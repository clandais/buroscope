﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuServiceEmploi : MonoBehaviour {

    public GameObject menuServiceEmploi;


    private void OnEnable()
    {
        menuServiceEmploi.SetActive(false);
    }

    public void Clicked()
    {
        if (!menuServiceEmploi.GetComponent<Animator>().GetBool("opened"))
        {
            menuServiceEmploi.SetActive(true);
            menuServiceEmploi.GetComponent<Animator>().SetBool("opened", true);
        }
        else
        {
            menuServiceEmploi.GetComponent<Animator>().SetBool("opened", false);
        }

    }


}
