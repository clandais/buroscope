﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScorePanel : MonoBehaviour
{


    public Sprite levelCompleteImage;
    public Image[] progressionImages;
    public float[,] formationGroupsScores;
    
    void Start()
    {
        formationGroupsScores = new float[7,4];
        for (int i = 0; i < progressionImages.Length; i++)
        {
            progressionImages[i].fillAmount = 0f;
        }
    }


    public void UpdateScores(int formationGroup, int formationId, float score, int courseCount)
    {
        formationGroupsScores[formationGroup - 1, formationId - 1] = score / courseCount;


        float fillAmount = 0f;

        for (int i = 0; i < courseCount; i++)
        {


            //compute score
            fillAmount += formationGroupsScores[formationGroup - 1, i];
            
        }

        if (fillAmount < 1)
            progressionImages[formationGroup - 1].fillAmount = fillAmount;
        else
        {
            progressionImages[formationGroup - 1].transform.FindChild("Badge").gameObject.SetActive(true);
        }

    }


}
