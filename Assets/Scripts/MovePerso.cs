﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class MovePerso : MonoBehaviour {

    public float angleOfView;

    public bool professor, canMove = true;

    public Transform professorT;

    public GameObject defaultCharacter;
    public GameObject circlePrefab;

    private UnityEngine.AI.NavMeshAgent navA;

    private GameObject character;

    private GameObject navigationIndicator;

    void Awake()
    {
        GameObject characterChoice = GameObject.Find("PersistentCharacterChoice");
        if (characterChoice != null)
        {
            character = Instantiate(characterChoice.GetComponent<PersistentCharacterChoice>().characterPrefab);
        }
        else
        {
            character = Instantiate(defaultCharacter);
        }
        character.transform.parent = this.transform;
        character.transform.localRotation = Quaternion.identity;
        character.transform.localScale = Vector3.one;
        character.transform.localPosition = Vector3.zero;
        character.AddComponent<AnimationGesture>();

        navA = GetComponent<UnityEngine.AI.NavMeshAgent>();
        canMove = true;
    }

    void Start()
    {
        character.GetComponent<AnimationGesture>().InitializeCharacter();
    }

    void Update ()
    {

        // if the character is < 0.1 meters from the indicator: hide it
        if (navigationIndicator != null && (transform.position - navigationIndicator.transform.position).magnitude < .2 )
        {
            navigationIndicator.SetActive(false);
        }
           
    }


    public void Move(Vector3 position)
    {
        if (canMove)
        { 
            navA.SetDestination(position);

            if (navigationIndicator == null)
            {
                navigationIndicator = (GameObject)Instantiate(circlePrefab, new Vector3(position.x, transform.position.y, position.z), Quaternion.Euler(90f, 0f, 0f));
            }

            else
            {
                navigationIndicator.transform.position = new Vector3(position.x, transform.position.y, position.z);
                navigationIndicator.SetActive(true);
            }

        }
    }
    
}
